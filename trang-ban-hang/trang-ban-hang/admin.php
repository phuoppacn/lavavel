<?php 
	session_start();
	//load file config
	include "config.php";
	//load file model
	include "model/model.php";
	//kiểm tra nếu user chưa đăng nhập thì hiển thị MVc login nếu user đã đăng nhập thì hiển thị file master trong backend
	if (isset($_SESSION["c_email"]) ==false)
		include "controller/backend/controller_login.php";
	else{
		//user đã đăng nhập
		include "view/backend/master.php";
	}
 ?>