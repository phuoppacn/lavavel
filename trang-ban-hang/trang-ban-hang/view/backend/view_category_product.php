<div class="row justify-content-center">
	<div class="col-md-10">
		<div style="margin-bottom: 5px;">
			<a href="admin.php?controller=add_edit_category_product&act=add" class="btn btn-primary">Add</a>
		</div>
		<div class="card border-primary">
			<div class="card-header bg-primary text-white">category_product</div>
			<div class="card-body">
				<table class="table table-hover table-bordered table-striped">
					<tr>
						<th>Category name</th>
						<th style="width: 150px;"></th>
					</tr>
					<?php 
						foreach($arr as $rows)
						{
					 ?>
					<tr>
						<td><?php echo $rows->c_name; ?></td>
						<td style="text-align: center;">
							<a href="admin.php?controller=add_edit_category_product&act=edit&id=<?php echo $rows->pk_category_product_id; ?>">Edit</a>&nbsp;&nbsp;
							<a href="admin.php?controller=category_product&act=delete&id=<?php echo $rows->pk_category_product_id; ?>">Delete</a>
						</td>
					</tr>
					<?php } ?>
				</table>
				<style type="text/css">
					.pagination{padding:0px; margin:0px;}
				</style>
				<ul class="pagination">
					<li class="page-item"><a class="page-link" href="#">Trang</a></li>
					<?php 
						for($i = 1; $i<= $num_page; $i++)
						{
					 ?>
					<li class="page-item"><a class="page-link" href="admin.php?controller=category_product&p=<?php echo $i; ?>"><?php echo $i; ?></a></li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</div>
</div>