<!DOCTYPE html>
<html lang="fr">
<head>
<!-- Site meta -->
<meta charset="utf-8">
<base href="http://thanhtest.byethost7.com/">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Ecommerce</title>
<?php include "function/remove_unicode.php"; ?>
<!-- CSS -->
<link href="public/frontend/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="public/frontend/css/fontawesome-all.min.css">
<link href="public/frontend/css/css.css?family=Open+Sans:400,300,600" rel="stylesheet" type="text/css">
<link href="public/frontend/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-info">
  <div class="container"> <a class="navbar-brand" href="index.html">Simple Ecommerce</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
    <div class="collapse navbar-collapse justify-content-end" id="navbarsExampleDefault">
      <ul class="navbar-nav m-auto">
        <li class="nav-item active"> <a class="nav-link" href="<?php echo remove_unicode("trang chủ"); ?>">Home <span class="sr-only">(current)</span></a> </li>
        <li class="nav-item active"> <a class="nav-link" href="<?php echo remove_unicode("giỏ hàng"); ?>">Card</a> </li>
        <li class="nav-item active"> <a class="nav-link" href="#contact">Contact</a> </li>
      </ul>
      <form class="form-inline my-2 my-lg-0">
        <div class="input-group input-group-sm">
          <input type="text" class="form-control" placeholder="Search...">
          <div class="input-group-append">
            <button type="button" class="btn btn-secondary btn-number"> <i class="fa fa-search"></i> </button>
          </div>
        </div>
        <a class="btn btn-success btn-sm ml-3" href="gio-hang"> <i class="fa fa-shopping-cart"></i> Cart <span class="badge badge-light"><?php if(isset($_SESSION["number_cart"])){echo $_SESSION["number_cart"]; }else {echo "0";} ?></span> </a>
      </form>
    </div>
  </div>
</nav>
<section style="background-image: url('public/frontend/images/banner.jpg'); height: 300px; margin-bottom: 20px;  border-top: 1px solid white"></section>
<?php 
  //lấy biến controller từ url
  $controller = isset($_GET["controller"])?$_GET["controller"]:"";
  //kiểm tra nếu $controller có giá trị và tồn tại controller đó
  if($controller !=""&&file_exists("controller/frontend/controller_$controller.php")){
    //ghi các thành phần để thành đường dẫn cật lý
    $controller="controller/frontend/controller_$controller.php";
  }else{
    $controller="controller/frontend/controller_home.php";
  }
  if(file_exists($controller))
    include $controller;
 ?>
<!-- Footer -->
<footer class="text-light">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-lg-4 col-xl-3">
        <h5>About</h5>
        <hr class="bg-white mb-2 mt-0 d-inline-block mx-auto w-25">
        <p class="mb-0"> Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. </p>
      </div>
      <div class="col-md-2 col-lg-2 col-xl-2 mx-auto">
        <h5>Informations</h5>
        <hr class="bg-white mb-2 mt-0 d-inline-block mx-auto w-25">
        <ul class="list-unstyled">
          <li><a href="">Link 1</a></li>
          <li><a href="">Link 2</a></li>
          <li><a href="">Link 3</a></li>
          <li><a href="">Link 4</a></li>
        </ul>
      </div>
      <div class="col-md-3 col-lg-2 col-xl-2 mx-auto">
        <h5>Others links</h5>
        <hr class="bg-white mb-2 mt-0 d-inline-block mx-auto w-25">
        <ul class="list-unstyled">
          <li><a href="">Link 1</a></li>
          <li><a href="">Link 2</a></li>
          <li><a href="">Link 3</a></li>
          <li><a href="">Link 4</a></li>
        </ul>
      </div>
      <div class="col-md-4 col-lg-3 col-xl-3" id="contact">
        <h5>Contact</h5>
        <hr class="bg-white mb-2 mt-0 d-inline-block mx-auto w-25">
        <ul class="list-unstyled">
          <li><i class="fa fa-home mr-2"></i> My company</li>
          <li><i class="fa fa-envelope mr-2"></i> email@example.com</li>
          <li><i class="fa fa-phone mr-2"></i> + 33 12 14 15 16</li>
          <li><i class="fa fa-print mr-2"></i> + 33 12 14 15 16</li>
        </ul>
      </div>
      <div class="col-12 copyright mt-3">
        <p class="float-left"> <a href="#">Back to top</a> </p>
        <p class="text-right text-muted">Devpro</p>
      </div>
    </div>
  </div>
</footer>

<!-- JS --> 
<script src="public/frontend/js/jquery-3.2.1.slim.min.js" type="text/javascript"></script> 
<script src="public/frontend/js/popper.min.js" type="text/javascript"></script> 
<script src="public/frontend/js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>
