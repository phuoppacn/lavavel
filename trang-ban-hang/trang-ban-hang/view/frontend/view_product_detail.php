<div class="container"> 
  <!-- main -->
  <div class="row">
    <div class="col">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Cras justo odio</li>
        </ol>
      </nav>
    </div>
  </div>
  <div class="row">
    <div class="col-12 col-sm-3">
      <div class="card bg-light mb-3">
        <div class="card-header bg-primary text-white text-uppercase"><i class="fa fa-list"></i> Categories</div>
        <?php include "controller/frontend/controller_category.php"; ?>
      </div>
      <div class="card bg-light mb-3">
        <div class="card-header bg-success text-white text-uppercase">Hot product</div>
        <div class="card-body"> 
          <!-- list hot product -->
          <?php 
            include "controller/frontend/controller_hot_product.php";
           ?>
          <!-- end list hot product --> 
        </div>
      </div>
    </div>
    <div class="col"> 
      <!-- product detail -->
      <div class="row"> 
        <!-- Image -->
        <div class="col-12 col-lg-6">
          <div class="box-detail" align="center"><img src="public/upload/product/<?php echo $product->c_img ?>" style=" width: 80%"></div>
        </div>
        
        <!-- Add to cart -->
        <div class="col-12 col-lg-6 add_to_cart_block">
          <div class="card bg-light mb-3">
            <div class="card-body">
              <p class="price"><?php echo number_format($product->c_price); ?> $</p>
              <?php if($product->c_oldprice!=0){ ?>
              <p class="price_discounted"><?php echo number_format($product->c_oldprice); ?> $</p>
              <?php } ?>
              <form method="get" action="index.php?controller=cart">
                <a href="gio-hang/them-san-pham/<?php echo remove_unicode($product->c_name); ?>/<?php echo $product->pk_product_id; ?>" class="btn btn-success btn-lg btn-block text-uppercase"> <i class="fa fa-shopping-cart"></i> Add To Cart </a>
              </form>
              <div class="product_rassurance">
                <ul class="list-inline">
                  <li class="list-inline-item"><i class="fa fa-truck fa-2x"></i><br/>
                    Fast delivery</li>
                  <li class="list-inline-item"><i class="fa fa-credit-card fa-2x"></i><br/>
                    Secure payment</li>
                  <li class="list-inline-item"><i class="fa fa-phone fa-2x"></i><br/>
                    +33 1 22 54 65 60</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row"> 
        <!-- Description -->
        <div class="col-12">
          <div class="card border-light mb-3">
            <div class="card-header bg-primary text-white text-uppercase"><i class="fa fa-align-justify"></i> Description</div>
            <div class="card-body">
              <p class="card-text text-justify"> <?php echo $product->c_description ?></p>
            </div>
          </div>
        </div>
        <!-- end Description --> 
      </div>
      <!-- end product detail --> 
    </div>
  </div>
  <!-- end main --> 
</div>