<div class="container">
  <div class="row"> 
    <!-- category -->
    <div class="col-12 col-md-3">
      <div class="card-header bg-primary text-white text-uppercase"><i class="fa fa-list"></i> Categories</div>
    <?php 
      include "controller/frontend/controller_category.php";
    ?>
    </div>
    <!-- end category --> 
    <!-- slide -->
    <div class="col">
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active"> <img class="d-block w-100" src="public/frontend/images/chicago.jpg" alt="First slide"> </div>
          <div class="carousel-item"> <img class="d-block w-100" src="public/frontend/images/la.jpg" alt="Second slide"> </div>
          <div class="carousel-item"> <img class="d-block w-100" src="public/frontend/images/ny.jpg" alt="Third slide"> </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
    </div>
    <!-- end slide --> 
  </div>
</div>

<!-- lates product -->
<div class="container mt-3">
  <div class="row">
    <div class="col-sm">
      <div class="card">
        <div class="card-header bg-primary text-white text-uppercase"> <i class="fa fa-star"></i> Sản phẩm mới nhất </div>
        <div class="card-body">
          <div class="row"> 
            <?php 
              foreach($arr_new as $rows){
             ?>
            <!-- list product -->
            <div class="col-sm">
              <div class="card"><div align="center"> <img class="card-img-top" src="public/upload/product/<?php echo $rows->c_img ?>" alt="<?php echo $rows->c_name ?>" style=" width: 80%;" ></div>
                <div class="card-body">
                  <h4 class="card-title"><a href="san-pham/chi-tiet/<?php echo remove_unicode($rows->c_name); ?>/<?php echo $rows->pk_product_id; ?>" title="View Product"><?php echo $rows->c_name; ?></a></h4>
                  <p class="card-text text-justify"><?php echo $rows->c_description; ?></p>
                  <div class="row">
                    <div class="col">
                      <p class="btn btn-danger btn-block"><?php echo number_format($rows->c_price) ?> $</p>
                    </div>
                    <div class="col"> <a href="gio-hang/them-san-pham/<?php   echo remove_unicode($rows->c_name); ?>/<?php  echo $rows->pk_product_id; ?>" class="btn btn-success btn-block">Add to cart</a> </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>
            <!-- end list product -->  
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end lates product --> 
<!-- hot product -->
<div class="container mt-3 mb-4">
  <div class="row">
    <div class="col-sm">
      <div class="card">
        <div class="card-header bg-primary text-white text-uppercase"> <i class="fa fa-trophy"></i> Sản phẩm nổi bật </div>
        <div class="card-body">
          <div class="row">
            <?php 
              foreach($arr_hot as $rows){
             ?>
            <!-- list product -->
            <div class="col-sm">
              <div class="card"> <img class="card-img-top" src="public/upload/product/<?php echo $rows->c_img ?>" alt="<?php echo $rows->c_name ?>">
                <div class="card-body">
                  <h4 class="card-title"><a href="san-pham/chi-tiet/<?php echo remove_unicode($rows->c_name); ?>/<?php echo $rows->pk_product_id; ?>" title="View Product"><?php echo $rows->c_name; ?></a></h4>
                  <p class="card-text text-justify"><?php echo $rows->c_description; ?></p>
                  <div class="row">
                    <div class="col">
                      <p class="btn btn-danger btn-block"><?php echo number_format($rows->c_price) ?> $</p>
                    </div>
                    <div class="col"> <a href="gio-hang/them-san-pham/<?php echo remove_unicode($rows->c_name); ?>/<?php echo $rows->pk_product_id; ?>" class="btn btn-success btn-block">Add to cart</a> </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>
            <!-- end list product -->  
          </div>
        </div>
      </div>
    </div>
  </div>
</div>