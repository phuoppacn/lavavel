<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//--------------
/*
	Form trong laravel
	- Trong file view bat buoc phan su dung the token: <input type="hidden" name="_token" value="{{ csrf_token() }}">
*/
//public/master
Route::get("master",function(){
	return view("php26.master");
});
//dinh nghia route public/trangchu
Route::get("trangchu",function(){
	return view("php26.trang_chu");
});
//dinh nghia route public/gioithieu
Route::get("gioithieu",function(){
	return view("php26.gioi_thieu");
});
//dinh nghia rout public/dodulieu
//ung voi route nao thi phai xay dung view cho route day (chua phai la load file master o day)
Route::get("dodulieu",function(){
	return view("php26.do_du_lieu");
});
//--------------
