<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//--------------
/*
	Form trong laravel
	- Trong file view bat buoc phan su dung the token: <input type="hidden" name="_token" value="{{ csrf_token() }}">
*/
/*
	- controller nam tai duong dan: App\Http\Controllers\cac fil controller
	- file controller phai co cau truc: tenfileController.php (co nghia la phai co chuc Controller gan kem ten file)
*/
//goi controller co ten la php26Controller.php
//cau truc de goi: tencontroller@tenham
/*
	- co 2 cach de tao controller
	- cach 1: lenh cmd: php artisan make:controller php26Controller
	- cach 2: tao bang tay
*/
Route::get("controller1","php26Controller@hello");
//truyen bien tren route, lay bien trong controller
Route::get("controller2/{a}/{b}","php26Controller@truyenbien");
//--------------
