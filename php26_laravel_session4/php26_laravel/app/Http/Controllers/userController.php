<?php 
	namespace App\Http\Controllers;
	use Illuminate\Http\Request;
	//trong controller, muon su dung doi tuong nao thi phai khai bao doi tuong do bang tu khoa use
	//doi tuong thao tac voi csdl
	use DB;
	//doi tuong de generate password
	use Hash;
	class userController extends Controller{
		//hien thi tat ca cac ban ghi
		public function show(){
			//lay tat ca cac ban ghi, co phan trang (4 ban ghi tren mot trang)
			$data["arr"] = DB::table("users")->orderBy("id","desc")->paginate(4);
			//goi view va truyen du lieu ra view
			return view("backend.list_user",$data);
		}
		//xoa ban ghi co id truyen tu url
		public function delete($id){
			//xoa ban ghi co id truyen vao
			//cach 1
			DB::table("users")->where("id","=",$id)->delete();
			//cach 2
			//DB::delete("delete from users where id=$id");
			//di chuyen den route public/admin/user/show
			return redirect(url('admin/user/show'));
		}
		//edit ban ghi co id truyen vao
		public function edit($id){
			//lay mot ban ghi co id truyen vao
			$data["arr"] = DB::table("users")->where("id","=",$id)->first();
			//goi view va truyen du lieu ra view
			return view("backend.add_edit_user",$data);
		}
		//chuc nang edit: khi user submit form
		//phai truyen vao doi tuong request de bat du lieu
		public function do_edit(request $request,$id){
			$name = $request->get("name");
			$email = $request->get("email");
			$password = $request->get("password");
			//update ban ghi co id truyen vao
			DB::table("users")->where("id","=",$id)->update(array("name"=>$name,"email"=>$email));
			//neu password khong rong thi update password
			if($password != ""){
				//ma hoa password
				$password = Hash::make($password);
				//update ban ghi co id truyen vao
				DB::table("users")->where("id","=",$id)->update(array("password"=>$password));
			}
			//di chuyen den route public/admin/user/show
			return redirect(url('admin/user/show'));
		}
		//add user
		public function add(){
			//goi view add_edit_user.blade.php
			return view("backend.add_edit_user");
		}
		//add: khi user an submit
		public function do_add(request $request){
			$name = $request->get("name");
			$email = $request->get("email");
			$password = $request->get("password");
			//ma hoa password
			$password = Hash::make($password);
			//add ban ghi
			DB::table("users")->insert(array("name"=>$name,"email"=>$email,"password"=>$password));
			//di chuyen den route public/admin/user/show
			return redirect(url('admin/user/show'));
		}
	}
 ?>