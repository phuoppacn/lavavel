<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
	Doi tuong Hash::make("noidung") se tao chuoi password trong laravel
*/
//khai chay lenh cmd:php artisan make:auth thi 2 dong ben duoi se duoc tao
Auth::routes();
//co the an dong nay di
//Route::get('/home', 'HomeController@index')->name('home');
//-----------------
//khi go duong dan /public/admin thi se truy cap den route /public/admin/user/show neu user da dang nhap, neu chua dang nhap thi xuat hien cua so dang nhap
Route::get("admin",function(){
	/*
		redirect -> di chuyen den mot route
		url("duong dan ao") -> tao route tren url
	*/
	return redirect(url("admin/user/show"));
});
//dang xuat khoi admin
Route::get("logout",function(){
	//dang xuat
	auth::logout();
	//di chuyen den rout admin
	return redirect(url("admin"));
});
//xay dung route cua phan backend, tat ca se nhom vao mot tag co ten la admin
Route::group(array("prefix"=>"admin","middleware"=>"auth"),function(){
	//----------------
	//hien thi danh sach cac ban ghi trong table users
	Route::get("user/show","userController@show");
	//xoa mot ban ghi
	Route::get("user/delete/{id}","userController@delete");
	//edit
	Route::get("user/edit/{id}","userController@edit");
	//edit post (khi an nut submit)
	Route::post("user/edit/{id}","userController@do_edit");
	//add
	Route::get("user/add","userController@add");
	//add post (khi an nut submit)
	Route::post("user/add","userController@do_add");
	//----------------
	//----------------
	//hien thi danh sach cac ban ghi trong table newss
	Route::get("news/show","newsController@show");
	//xoa mot ban ghi
	Route::get("news/delete/{id}","newsController@delete");
	//edit
	Route::get("news/edit/{id}","newsController@edit");
	//edit post (khi an nut submit)
	Route::post("news/edit/{id}","newsController@do_edit");
	//add
	Route::get("news/add","newsController@add");
	//add post (khi an nut submit)
	Route::post("news/add","newsController@do_add");
	//----------------
});
//-----------------
