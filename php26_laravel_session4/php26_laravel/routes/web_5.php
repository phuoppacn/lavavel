<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//--------------
/*
	Goi view:
		- Cac file view nam tai duong dan: resources\views\file.blade.php
		- File view hoat dong dua tren template engine: blade engine
	Cu phap:
		Route::get("duong-dan-ao",function(){
			//goi view bang tu khoa return
			return view("tenview");
		})
		- Khi view dat trong mot thu muc, luc do goi view se theo cu phap: return view(tenthumuc.tenview)
	Truyen bien ra view:
		Route::get("duong-dan-ao",function(){
			//cac bien dat vao trong mot array
			$arr = array();
			$arr["a"] = giatri1;
			$arr["b"] = giatri2;
			//truyen array ra view
			return view("tenview",$arr);-> khi do o view se goi cac bien la $a, $b (laravel su dung ham extract de bien key cua array thanh ten bien)
		});
*/
	//url: public/view1
	Route::get("view1",function(){
		//goi view co ten la: testview1.blade.php
		return view("testview1");
	});
	//url: public/view2
	Route::get("view2",function(){
		//goi view co ten la: testview2.blade.php nam trong thu muc php26
		return view("php26.testview2");
	});
	//url: public/view3
	Route::get("view3",function(){
		return view("php26.testview3");
	});
	//truyen bien ra view
	//url: public/view4
	Route::get("view4",function(){
		$arr = array();
		$arr["hoten"] = "Nguyễn Văn A";
		$arr["email"] = "nva@mail.com";
		//goi view va truyen du lieu ra view
		return view("php26.testview4",$arr);
	});
//--------------
