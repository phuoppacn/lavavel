<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
	Laravel co the nhom route co chung ten. VD: 
	public/admin/tintuc
	public/admin/sanpham
	khi do se group lai thanh tag co ten la admin
*/
Route::group(array("prefix"=>"admin"),function(){
	//thiet lap cac route dang sau tag admin
	Route::get("tintuc",function(){
		echo "<h1>Trang Tin tức</h1>";
	});
	//-----------
	Route::get("sanpham",function(){
		echo "<h1>Trang Sản phẩm</h1>";
	});
	//-----------
});
