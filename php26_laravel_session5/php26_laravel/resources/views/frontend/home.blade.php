<!-- load template master.blade.php -->
@extends('frontend.master')
@section('do-du-lieu')
<!-- list category tin tuc -->
<?php 
    $category = DB::select("select * from tbl_category_news order by pk_category_news_id desc");
    //$category = DB::table("tbl_category_news")->orderBy("pk_category_news_id","desc")->get();
 ?>
 @foreach($category as $rows_category)
 <!-- kiem tra, neu danh muc nao co tin tuc thi moi hien thi -->
 <?php 
 	//lay so luong ban ghi trong table tbl_news thuoc cac danh muc tuong ung (su dung ham Count())
 	$check = DB::table("tbl_news")->where("fk_category_news_id","=",$rows_category->pk_category_news_id)->Count();
  ?>
  @if($check > 0)
                    <div class="row-fluid">
                        <div class="marked-title">
                            <h3><a href="#" style="color:white">{{ $rows_category->c_name }}</a></h3>
                        </div>
                    </div>                    
                    <div class="row-fluid">
                        <div class="span2">
<?php 
	//lay tin dau tien
	//$first_news = DB::select("select * from tbl_news order by pk_news_id desc limit 0,1"); -> phai dung foreach de duyet
	//lay 1 ban ghi
	$first_news = DB::table("tbl_news")->where("fk_category_news_id","=",$rows_category->pk_category_news_id)->orderBy("pk_news_id","desc")->limit(0,1)->first(); //-> xuat luon ket qua ma ko can dung foreach
 ?>                        
                           <!-- first news -->
                            <article>
                                <div class="post-thumb">
                                    <a href="{{ url('news/detail/'.$first_news->pk_news_id) }}"><img src="{{ asset('upload/news/'.$first_news->c_img) }}" alt=""></a>
                                </div>
                                <div class="cat-post-desc">

                                    <h3><a href="{{ url('news/detail/'.$first_news->pk_news_id) }}">{{ $first_news->c_name }}</a></h3>
                                    <!-- neu trong admin su dung ckeditor thi o day phai xuat nguyen mau ky tu html -->
                                    <p>{!! $first_news->c_description !!}</p>
                                </div>
                            </article>
                            <!-- end first news -->
                        </div>
                        <div class="span2">
                    <?php 
                    	$other_news = DB::table("tbl_news")->where("fk_category_news_id","=",$rows_category->pk_category_news_id)->orderBy("pk_news_id","desc")->offset(1)->limit(3)->get();
                    	//->offset(1)->tu ban ghi thu 1
                    	//limit(3) -> lay 3 ban ghi
                     ?>
                     @foreach($other_news as $rows)
                           <!-- list news -->
                            <article class="twoboxes">
                                <div class="right-desc">
                                    <h3><a href="{{ url('news/detail/'.$rows->pk_news_id) }}"><img src="{{ asset('upload/news/'.$rows->c_img) }}">{{ $rows->c_name }}</a></h3>  
                                    <div class="clear"></div>    
                                </div>
                                <div class="clear"></div>
                            </article>
                            <!-- end list news -->
					@endforeach
                        </div>
                    </div>
                    <div class="clear"></div>
                    <!-- end list category tin tuc -->
        @endif
	@endforeach                    

@endsection