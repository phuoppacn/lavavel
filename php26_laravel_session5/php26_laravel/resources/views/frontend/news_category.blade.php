@extends('frontend.master')
@section('do-du-lieu')
<?php 
	$category = DB::table("tbl_category_news")->where("pk_category_news_id","=",$pk_category_news_id)->first();
 ?>
<div class="marked-title">
                        <h3>{{ $category->c_name }}</h3>
                    </div>
                    <div class="row">
                <?php 
                	$news = DB::table("tbl_news")->where("fk_category_news_id","=",$pk_category_news_id)->paginate(3);
                 ?>
                 @foreach($news as $rows)
                        <!-- list news -->
                        <article>
							<div class="cat-post-desc">
								<h3><a href="{{ url('news/detail/'.$rows->pk_news_id) }}">{{ $rows->c_name }}</a></h3>
								<p><a href="{{ url('news/detail/'.$rows->pk_news_id) }}"><img class="img_category" src="{{ asset('upload/news/'.$rows->c_img) }}" alt=""></a> {!! $rows->c_description !!}</p>
							</div>
							<div class="clear"></div>
							<div class="line_category"></div>
						</article>                       
                        <!-- end list news -->
                  @endforeach                                                              
                                                
                    </div>
                    <div class="clear"></div>
                    <!-- phan trang bang ham render hoac link -->
                    {{ $news->render() }}
                    <!-- <div class="post-navi">
                        <ul>
                            <li><a href="#">&lt;</a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">...</a></li>
                            <li><a href="#">42</a></li>
                            <li><a href="#">43</a></li>
                            <li><a href="#">&gt;</a></li>
                        </ul>
                    </div> -->
@endsection