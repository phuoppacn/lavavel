<!DOCTYPE html>
<html>
<head>
	<title>Master</title>
	<meta charset="utf-8">
</head>
<body>
<style type="text/css">
	/* * la dai dien cho tat ca cac the html */
	*{padding:0px; margin:0px;}
	body{font-family: Arial;}
	header{height: 100px; background-color: red;}
	.wrap{width: 1000px; margin:auto; border:1px solid #dddddd;}
	aside.left{width: 200px; height: 400px; background-color: green; float: left;}
	section.main{width: 800px; height: 400px; float: left;}
	footer{height: 100px; background-color: purple; clear: both;}
	nav ul{list-style: none; background-color: black;}
	nav ul li{display: inline;}
	nav ul li a{color:white; text-decoration: none; padding:10px; line-height: 35px;}
</style>
<div class="wrap">
	<header>
		@yield('header')
	</header>
	<nav>
		<ul>
		<!-- 
			ham url("duong dan ao") se thuc hien ben trong the a de truyen len url duong dan ao (co nghia la link ben trong the a thi dung voi ham url)
		 -->
			<li><a href="{{ url('trangchu') }}">Trang chủ</a></li>
			<li><a href="{{ url('gioithieu') }}">Giới thiệu</a></li>
			<li><a href="#">Tin tức</a></li>
			<li><a href="#">Liên hệ</a></li>
		</ul>
	</nav>
	<aside class="left"><h1>Left</h1></aside>
	<section class="main">
		<!-- khai bao tag de cac view do du lieu vao day -->
		@yield('dodulieu')
	</section>
	<footer><h1>Footer</h1></footer>
</div>
</body>
</html>