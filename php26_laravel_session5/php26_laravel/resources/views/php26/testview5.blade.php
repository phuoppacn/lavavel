<!DOCTYPE html>
<html>
<head>
	<title>view 5</title>
	<meta charset="utf-8">
</head>
<body>
<h1>{{ "Hello world" }}</h1>
<?php 
	$hoten = "Nguyen Van A";
 ?>
 <h1>{{ "Họ tên: ".$hoten }}</h1>
 <h1>{!! "PHP26" !!}</h1>
 <h1>{{ "<" }} - {!! "<" !!}</h1>
 @for($i =1; $i < 5; $i++)
 <div>{{ $i }}</div>
 @endfor
 <?php 
 	$arr = array("IPhone","HTC","Samsung","Nokia");
  ?>
 @foreach($arr as $value)
 <h1>{{ $value }}</h1>
 @endforeach
</body>
</html>