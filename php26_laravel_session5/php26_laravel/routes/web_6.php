<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//--------------
/*
	Cu phap cua template engine: blade tempalte
	Co nghia la trong file view co the su dung duoc cu phap cua blade engine
	- Xuat thong bao len man hinh: {{ "noidung" hoac bien }} -> lenh nay tuong duong lenh echo
	- Xuat thong bao len man hinh: {!! "noidung" hoac bien !!} -> lenh nay tuong duong lenh echo
	- su dung vong lap for
		@for($i = 1; $i <= 10; $++)
		code html
		@endfor
	- su dung vong lap foreach
		@foreach($arr as $value)
		code html
		@endforeach
	- su dung lenh if
		@if(dieukien)
			html
		@else
			html
		@endif
	- tuong tu cho while va do while
*/
	//url: public/view5
	Route::get("view5",function(){
		return view("php26.testview5");
	});
//--------------
