<!DOCTYPE html>
<html>
<head>
	<title>post</title>
	<meta charset="utf-8">
</head>
<body>
<fieldset style="width: 300px; margin:auto;">
	<form method="post" action="">
		<!-- bat buoc phai co the formcontrol co ten la token -->
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<table cellpadding="5">
			<tr>
				<td>Họ và tên</td>
				<td><input type="text" name="hovaten" required></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><input type="text" name="email" required></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="POST"></td>
			</tr>
		</table>
		<h1>{{ isset($hovaten) ? $hovaten : "" }}</h1>
		<h1>{{ isset($email) ? $email : "" }}</h1>
	</form>
</fieldset>
</body>
</html>