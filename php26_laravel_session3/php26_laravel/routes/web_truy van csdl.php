<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//--------------
/*
	Form trong laravel
	- Trong file view bat buoc phan su dung the token: <input type="hidden" name="_token" value="{{ csrf_token() }}">
*/
/*
	- Ket noi csdl: thong tin ket noi nam o file .env (thay doi cac thong so tu dong 9 den dong 13)
	- Chay lenh cmd: php artisan migrate (khi do laravel se doc du lieu trong thu muc database\migrations de tao table users, table migrations). Neu khi lam project khac ma da co cac table nay trong csdl thi khong can chay lenh nay
*/
Route::get("csdl1",function(){
	//liet ke tat ca cac ban ghi
	$data["arr"] = DB::table("tbl_test")->get();
	foreach($data["arr"] as $rows){
		echo "<div>".$rows->hovaten."</div>";
	}
});
/*
	Truy van csdl
	- Lay tat ca cac ban ghi
	DB::table("ten")->get();
	- Lay cac ban ghi theo dieu kien where
	DB::table("ten")->where("tencot","dieukien(> < = !=)","giatri")->get();
	- Lay mot ban ghi
	DB::table("ten")->first();
	- Lay mot ban ghi theo dieu kien where
	DB::table("ten")->where("tencot","dieukien(> < = !=)","giatri")->first();
	- chon cac cot de lay ra
	DB::table("ten")->where("tencot","dieukien(> < = !=)","giatri")->select(cot1,cot2...)->get();
	- Menh de order by
	DB::table("ten")->where("tencot","dieukien(> < = !=)","giatri")->orderBy("tencot","asc/desc")->get();
	- insert
	DB::table("ten")->insert(array("tencot1"=>"giatri1","tencot2"=>"giatri2"))
	- update
	DB::table("ten")->where("tencot","dieukien(> < = !=)","giatri")->update(array("tencot1"=>"giatri1","tencot2"=>"giatri2"))
	- delete
	DB::table("ten")->where("tencot","dieukien(> < = !=)","giatri")->delete()
*/
//--------------
