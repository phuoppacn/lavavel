<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//--------------
// cấu hình đường dẫn: public/php26/hello
Route::get("php26/hello",function(){
	echo "<h1>Hello world</h1>";
});
//--------------
