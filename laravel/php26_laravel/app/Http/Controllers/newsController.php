<?php 
	namespace App\Http\Controllers;
	use Illuminate\Http\Request;
	//su dung doi tuong DB de thao tac csdl, phai khai bao su dung no bang tu khoa use
	use DB;
	class newsController extends Controller{
		public function show(){
			//lay tat cac cac ban ghi, phan 5 ban ghi tren 1 trang
			$data["arr"] = DB::table("tbl_news")->orderBy("pk_news_id","desc")->paginate(5);
			//goi view va dua du lieu ra view
			return view("backend.list_news",$data);
		}
		//edit news
		public function edit($id){
			//lay 1 ban ghi tuong ung voi id truyen vao
			$data["arr"] = DB::table("tbl_news")->where("pk_news_id","=",$id)->first();
			//goi view va truyen du lieu ra view
			return view("backend.add_edit_news",$data);
		}
		//do edit
		public function do_edit(Request $request, $id){
			$c_name = $request->get("c_name");
			$c_description = $request->get("c_description");
			$c_content = $request->get("c_content");
			$fk_category_news_id = $request->get("fk_category_news_id");
			$c_hotnews = $request->get("c_hotnews") != "" ? 1 : 0;
			//update ban ghi co id truyen vao
			DB::table("tbl_news")->where("pk_news_id","=",$id)->update(array("c_name"=>$c_name,"c_description"=>$c_description,"c_content"=>$c_content,"fk_category_news_id"=>$fk_category_news_id,"c_hotnews"=>$c_hotnews));
			//-------------
			//kiem tra xem user co chon anh hay khong
			if($request->hasFile("c_img")){
				//------------
				//xoa anh cu truoc khi update anh moi
				//->select("cot1","cot2"...) se lay cac cot duoc chi dinh 
				$old_img = DB::table("tbl_news")->where("pk_news_id","=",$id)->select("c_img")->first();
				//thuc hien xoa anh cu
				if(file_exists('upload/news/'.$old_img->c_img))
					unlink('upload/news/'.$old_img->c_img);
				//------------
				//lay ten anh
				$c_img = time().$request->file("c_img")->getClientOriginalName();
				//thuc hien upload anh
				$request->file("c_img")->move("upload/news/",$c_img);
				//update cot c_img
				DB::table("tbl_news")->where("pk_news_id","=",$id)->update(array("c_img"=>$c_img));
			}
			//-------------
			//di chuyen den route admin/news/show
			return redirect(url("admin/news/show"));
		}
		//add
		public function add(){
			//goi view
			return view("backend.add_edit_news");
		}
		//do add
		public function do_add(Request $request){
			$c_name = $request->get("c_name");
			$c_description = $request->get("c_description");
			$c_content = $request->get("c_content");
			$fk_category_news_id = $request->get("fk_category_news_id");
			$c_hotnews = $request->get("c_hotnews") != "" ? 1 : 0;
			$c_img = "";
			//neu user chon anh
			if($request->hasFile("c_img")){
				//lay ten anh
				$c_img = time().$request->file("c_img")->getClientOriginalName();
				//thuc hien upload file
				$request->file("c_img")->move("upload/news",$c_img);
			}
			//insert ban ghi
			DB::table("tbl_news")->insert(array("c_name"=>$c_name,"c_description"=>$c_description,"c_content"=>$c_content,"fk_category_news_id"=>$fk_category_news_id,"c_hotnews"=>$c_hotnews,"c_img"=>$c_img));
			//di chuyen den route admin/news/show
			return redirect(url("admin/news/show"));
		}
		//xoa ban ghi
		public function delete($id){
			//------------
			//xoa anh cu truoc khi update anh moi
			//->select("cot1","cot2"...) se lay cac cot duoc chi dinh 
			$old_img = DB::table("tbl_news")->where("pk_news_id","=",$id)->select("c_img")->first();
			//thuc hien xoa anh cu
			if(file_exists('upload/news/'.$old_img->c_img))
				unlink('upload/news/'.$old_img->c_img);
			//------------
			//xoa ban ghi co id truyen vao
			DB::table("tbl_news")->where("pk_news_id","=",$id)->delete();
			//di chuyen den route admin/news/show
			return redirect(url("admin/news/show"));
		}
	}

 ?>