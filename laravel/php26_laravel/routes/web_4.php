<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//--------------
/*
	Truyền biến lên url
	Route::get('duong-dan-ao/{bien1}/{bien2}',function($bien1,$bien2){code});
*/
	//goi duong dan public/truyenbien/5/hello -> xuat 2 bien 5, hello len man hinh
	Route::get("truyenbien/{bien1}/{bien2}",function($bien1,$bien2){
		echo "<h1>Biến 1: $bien1</h1>";
		echo "<h1>Biến 2: $bien2</h1>";
	});
	//goi duong dan: tinhtong/3-4-5 -> hiển thị: 3 + 4 + 5 = 12
	Route::get("tinhtong/{bien1}-{bien2}-{bien3}",function($b1,$b2,$b3){
		$tong = $b1 + $b2 + $b3;
		echo "<h1>$b1 + $b2 + $b3 = $tong</h1>";
	});
//--------------
