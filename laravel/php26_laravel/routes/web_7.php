<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//--------------
/*
	Form trong laravel
	- Trong file view bat buoc phan su dung the token: <input type="hidden" name="_token" value="{{ csrf_token() }}">
*/
	//url: public/form1
	Route::get("form1",function(){
		return view("php26.testform1");
	});
	//khi an nut submit thi trang thai cua trang se la POST, khi do su dung phuong thuc POST de lay du lieu
	Route::post("form1",function(){
		//Bat bien formcontrol bang cu phap: Request::get("tenformcontrol")
		$arr["hovaten"] = Request::get("hovaten");
		$arr["email"] = Request::get("email");
		echo "<h1>".$arr["hovaten"]."</h1>";
		echo "<h1>".$arr["email"]."</h1>";
		return view("php26.testform1",$arr);
	});
//--------------
