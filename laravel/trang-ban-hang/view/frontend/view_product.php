<div class="container"> 
  <!-- main -->
  <div class="row">
    <div class="col">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Cras justo odio</li>
        </ol>
      </nav>
    </div>
  </div>
  <div class="row">
    <div class="col-12 col-sm-3">
      <div class="card bg-light mb-3">
        <div class="card-header bg-primary text-white text-uppercase"><i class="fa fa-list"></i> Categories</div>
        <?php include "controller/frontend/controller_category.php" ?>
      </div>
      <div class="card bg-light mb-3">
        <div class="card-header bg-success text-white text-uppercase">Hot product</div>
        <div class="card-body"> 
          <!-- list hot product -->
          <?php 
            include "controller/frontend/controller_hot_product.php";
           ?>
          <!-- end list hot product -->
        </div>
      </div>
    </div>
    <div class="col">
      <div class="row"> 
        <!-- list product -->
        <?php 
          foreach ($arr as $rows){
         ?>
        <div class="col-12 col-md-6 col-lg-4" style="margin-bottom: 20px;">
          <div class="card" > <div align="center"><img class="card-img-top" src="public/upload/product/<?php echo $rows->c_img ?>" alt="<?php echo $rows->c_name ?>" style=" width: 80%"></div>
            <div class="card-body">
              <h4 class="card-title"><a href="san-pham/chi-tiet/<?php echo remove_unicode($rows->pk_product_id); ?>/<?php echo $rows->pk_product_id; ?>" title="View Product"><?php echo $rows->c_name; ?></a></h4>
              <p class="card-text text-justify"><?php echo $rows->c_description; ?></p>
              <div class="row">
                <div class="col">
                  <p class="btn btn-danger btn-block"><?php echo number_format($rows->c_price); ?> $</p>
                </div>
                <div class="col"> <a href="gio-hang/them-san-pham/<?php echo remove_unicode($rows->c_name); ?>/<?php echo $rows->pk_product_id; ?>" class="btn btn-success btn-block">Add to cart</a> </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <!-- end list product --> 
        
        <?php 
          $number_page= isset($_GET["p"])?$_GET["p"]:1;
          if($number_page=1)
            $display="none";
          $id_category=$this->model->get_a_record("select * from tbl_category_product where pk_category_product_id=$rows->fk_category_product_id");
         ?>
        <div class="col-12">
          <nav aria-label="...">
            <ul class="pagination">
              <li class="page-item disabled"> <a class="page-link" href="san-pham/danh-muc/<?php echo $id_category->c_name ?>/<?php $rows->fk_category_product_id; ?>/<?php echo $number_page-2; ?>" tabindex="-1">Previous</a> </li>
              <li style="display: <?php echo $display; ?>" class="page-item"><a class="page-link" href="san-pham/danh-muc/<?php echo $id_category->c_name ?>/<?php $rows->fk_category_product_id; ?>/<?php echo $number_page-1; ?>"><?php echo $number_page-1 ?></a></li>
              <li style="display: <?php echo $display_end ?>" class="page-item active"> <a class="page-link" href="san-pham/danh-muc/<?php echo $id_category->c_name ?>/<?php $rows->fk_category_product_id; ?>/<?php echo $number_page; ?>"><?php echo $number_page ?> <span class="sr-only">(current)</span></a> </li>
              <li class="page-item"><a class="page-link" href="san-pham/danh-muc/<?php echo $id_category->c_name ?>/<?php $rows->fk_category_product_id; ?>/<?php echo $number_page+1; ?>"><?php echo $number_page+1 ?></a></li>
              <li style="display: <?php echo $display_end ?>" class="page-item"> <a class="page-link" href="san-pham/danh-muc/<?php echo $id_category->c_name ?>/<?php $rows->fk_category_product_id; ?>/<?php echo $number_page+2; ?>">Next</a> </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
  <!-- end main --> 
</div>