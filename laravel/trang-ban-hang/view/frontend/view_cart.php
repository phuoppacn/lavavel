<div class="container"> 
  <!-- main -->
  <div class="row">
    <div class="col">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Cras justo odio</li>
        </ol>
      </nav>
    </div>
  </div>
  <div class="row">
    <div class="col-12 col-sm-3">
      <div class="card bg-light mb-3">
        <div class="card-header bg-primary text-white text-uppercase"><i class="fa fa-list"></i> Categories</div>
        <?php 
          include "controller/frontend/controller_category.php";
         ?>
      </div>
      <div class="card bg-light mb-3">
        <div class="card-header bg-success text-white text-uppercase">Hot product</div>
        <div class="card-body"> 
          <!-- list hot product -->
          <?php 
            include "controller/frontend/controller_hot_product.php";
           ?>
          <!-- end list hot product -->
        </div>
      </div>
    </div>
    <div class="col"> 
      <!-- card -->
      <form method="post" action="">
        <div class="row">
          <div class="col-12">
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th scope="col"> </th>
                    <th scope="col">Product</th>
                    <th scope="col" class="text-center">Quantity</th>
                    <th scope="col" class="text-right">Price</th>
                    <th> </th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                    foreach($_SESSION["cart"] as $product){

                   ?>
                  <tr>
                    <td><img src="public/upload/product/<?php  echo $product["c_img"];  ?>" /></td>
                    <td><?php echo $product["c_name"]; ?></td>
                    <td><input name="product_<?php echo $product["pk_product_id"];?>" type="number" style=" width: 80px;" min="0" value="<?php echo $product["number"]; ?>" class="form-control text-center"/></td>
                    <td class="text-right"><?php echo number_format($product["c_price"]); ?> €</td>
                    <td class="text-right"><button class="btn btn-sm btn-danger"><a style=" text-decoration: none;color: #fff" href="gio-hang/xoa-san-pham/<?php echo remove_unicode($product['c_name']); ?>/<?php echo $product["pk_product_id"] ?>"><i class="fa fa-trash"></i></a> </button></td>
                  </tr>
                  <?php } ?>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><strong>Total number of product</strong></td>
                    <td class="text-right"><?php $_SESSION["number_cart"]= number_format($this->cart_number()); 
                    echo $_SESSION["number_cart"];
                    ?> SP</td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><strong>Total cost</strong></td>
                    <td class="text-right"><?php echo number_format($this->cart_total()); ?> €</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="col mb-2">
            <div class="row">
              <div class="col-md-12 col-sm-6 text-right"> <a href="index.php" class="btn btn-primary">Continue shopping</a> <a href="#" class="btn btn-danger">Check out</a> </div>
            </div>
          </div>
        </div>
      </form>
      <!-- end card --> 
    </div>
  </div>
  <!-- end main --> 
</div>